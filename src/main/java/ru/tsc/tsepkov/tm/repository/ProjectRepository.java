package ru.tsc.tsepkov.tm.repository;

import ru.tsc.tsepkov.tm.api.IProjectRepository;
import ru.tsc.tsepkov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tsepkov.tm.model.Project;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        final List<Project> result = new ArrayList<>(projects);
        result.sort(comparator);
        return result;
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project: projects) {
            if (id.equals(project.getId())) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public boolean existsById(final String id) {
        for (final Project project: projects) {
            if (id.equals(project.getId())) return true;
        }
        return false;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public int getSize() {
        return projects.size();
    }

}
