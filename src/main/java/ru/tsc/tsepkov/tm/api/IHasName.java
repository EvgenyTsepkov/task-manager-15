package ru.tsc.tsepkov.tm.api;

public interface IHasName {

    String getName();

    void setName(String name);

}
