package ru.tsc.tsepkov.tm.exception.system;

import ru.tsc.tsepkov.tm.exception.AbstractException;

public abstract class AbstractSystemException extends AbstractException {

    public AbstractSystemException() {
    }

    public AbstractSystemException(final String message) {
        super(message);
    }

    public AbstractSystemException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractSystemException(final Throwable cause) {
        super(cause);
    }

    public AbstractSystemException(final String message,final Throwable cause,final boolean enableSuppression,final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
